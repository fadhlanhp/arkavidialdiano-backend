from .models import Bibit, Investor, Investasi
from .serializers import BibitSerializer, UserSerializer, InvestorSerializer, InvestasiSerializer
from .permissions import IsygInvestOrReadOnly

from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.http import Http404
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt

from rest_framework import generics
from rest_framework import status
from rest_framework import permissions
from rest_framework.authtoken.models import Token
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.status import (
    HTTP_400_BAD_REQUEST,
    HTTP_404_NOT_FOUND,
    HTTP_200_OK
)
from rest_framework.views import APIView

class BibitList(APIView):
    def get(self, request, format=None):
        bibits = Bibit.objects.all()
        serializer = BibitSerializer(bibits, many=True)
        return Response(serializer.data)

class InvestorList(generics.ListAPIView):
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]
    queryset = Investor.objects.all()
    serializer_class = InvestorSerializer
    def perform_create(self, serializer):
        serializer.save(ygInvest=self.request.user)

class InvestorDetail(generics.RetrieveAPIView):
    permission_classes = [permissions.IsAuthenticatedOrReadOnly,
                      IsygInvestOrReadOnly]
    queryset = Investor.objects.all()
    serializer_class = InvestorSerializer
    
class UserList(generics.ListAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer

class UserDetail(generics.RetrieveAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer

class BibitDetail(APIView):
    def get_object(self, pk):
        try:
            return Bibit.objects.get(pk=pk)
        except Bibit.DoesNotExist:
            raise Http404
    
    def get(self, request, pk, format=None):
        bibit = self.get_object(pk)
        bibit = BibitSerializer(bibit)
        return Response(bibit.data)

class InvestasiList(APIView):
    def post(self, request):
        serializer = InvestasiSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class InvestasiperUser(APIView):
    def get(self, request, pk, format=None):
        investasisemua = Investasi.objects.all().filter(investor__id = pk).filter(status = 'Aktif')
        serializer = InvestasiSerializer(investasisemua, many=True)
        return Response(serializer.data)

class InvestasiperUserDetail(APIView):
    def get_object(self, pk2):
        try:
            return Investasi.objects.get(pk=pk2)
        except Investasi.DoesNotExist:
            raise Http404

    def get(self, request, pk, pk2, format=None):
        investasian = self.get_object(pk2)
        serializer = InvestasiSerializer(investasian)
        return Response(serializer.data)    

@csrf_exempt
@api_view(["POST"])
@permission_classes((AllowAny,))
def login(request):
    username = request.data.get("username")
    password = request.data.get("password")
    if username is None or password is None:
        return Response({'error': 'Please provide both username and password'},
                        status=HTTP_400_BAD_REQUEST)
    user = authenticate(username=username, password=password)
    if not user:
        return Response({'error': 'Invalid Credentials'},
                        status=HTTP_404_NOT_FOUND)
    token, _ = Token.objects.get_or_create(user=user)
    return Response({'token': token.key},
                    status=HTTP_200_OK)
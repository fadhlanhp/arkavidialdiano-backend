from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

# Create your models here.

class Bibit(models.Model):
    nama = models.CharField(max_length=50, null=False)
    hargaBibit = models.IntegerField()
    persen = models.IntegerField()
    durasi = models.IntegerField()
    listrisiko = [
        ('Rendah', 'Rendah'),
        ('Sedang', 'Sedang'),
        ('Tinggi', 'Tinggi')
    ]
    risiko = models.CharField(max_length=7, choices=listrisiko, default='Rendah')
    rating = models.IntegerField()
    jumlahPenjualan = models.IntegerField()
    namaOwner = models.CharField(max_length=50)
    jasa = models.IntegerField()
    alat = models.IntegerField()
    pupuk = models.IntegerField()
    image1 = models.CharField(max_length=50, null=True)
    image2 = models.CharField(max_length=50, null=True)
    image3 = models.CharField(max_length=50, null=True)

class Investor(models.Model):
    ygInvest = models.OneToOneField('auth.User', related_name='investors', on_delete=models.CASCADE)
    keuntungan = models.IntegerField()
    nama = models.CharField(max_length=50, null=False)
    email = models.EmailField(max_length=100, null=False)

class Investasi(models.Model):
    investor = models.ForeignKey(Investor, on_delete=models.CASCADE)
    bibit = models.ForeignKey(Bibit, on_delete=models.CASCADE)
    liststatus = [
        ('Aktif', 'Aktif'),
        ('Selesai', 'Selesai')
    ]
    status = models.CharField(max_length=8, choices=liststatus, default='Aktif')
    totalInvest = models.IntegerField()
    totalKeuntungan = models.IntegerField()

    # @property
    # def total(self):
    #     return self.keuntungan/100 * jumlahBibit

    # @property
    # def total(self):
    #     return
 

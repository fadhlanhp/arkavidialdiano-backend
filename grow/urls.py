from django.urls import path
from .views import *

urlpatterns = [
    path('bibit/', BibitList.as_view()),
    path('bibit/<int:pk>', BibitDetail.as_view()),
    path('login/', login),
    path('profile/', InvestorList.as_view()),
    path('users/', UserList.as_view()),
    path('users/<int:pk>/', UserDetail.as_view()),
    path('investasi/', InvestasiList.as_view()),
    path('investasi/users/<int:pk>', InvestasiperUser.as_view()),
    path('investasi/users/<int:pk>/<int:pk2>', InvestasiperUserDetail.as_view()),
    #path('investasiaktif/users/<int:pk>', Investasiaktif.as_view())
]
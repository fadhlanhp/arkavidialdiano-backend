from .models import Bibit, Investor, Investasi
from rest_framework import serializers
from django.contrib.auth.models import User

class BibitSerializer(serializers.ModelSerializer):
    class Meta:
        model = Bibit
        fields = ('nama', 'hargaBibit', 'persen', 'durasi', 'risiko', 'rating', 'jumlahPenjualan', 'namaOwner', 'jasa', 'alat', 'pupuk', 'image1', 'image2', 'image3')

class InvestorSerializer(serializers.ModelSerializer):
    class Meta:
        ygInvest = serializers.ReadOnlyField(source='ygInvest.username')
        model = Investor
        fields = ('ygInvest', 'nama', 'email', 'keuntungan')

class UserSerializer(serializers.ModelSerializer):
    investors = serializers.PrimaryKeyRelatedField(many=True, queryset=Investor.objects.all())
    class Meta:
        model = User
        fields = ['username', 'investors']

class InvestasiSerializer(serializers.ModelSerializer):
    investor_id = serializers.PrimaryKeyRelatedField(queryset=Investor.objects.all(), write_only=True)
    investor = serializers.PrimaryKeyRelatedField(source='investor.id', read_only=True)
    bibit_id = serializers.PrimaryKeyRelatedField(queryset=Bibit.objects.all(), write_only=True)
    bibit = serializers.PrimaryKeyRelatedField(source='bibit.id', read_only=True)

    class Meta:
        model = Investasi
        fields = ('investor_id', 'bibit_id', 'investor', 'bibit', 'status', 'totalInvest', 'totalKeuntungan')

    def create(self, validated_data):
        investor = validated_data.pop('investor_id')
        bibit = validated_data.pop('bibit_id')
        investasi = Investasi.objects.create(investor=investor, bibit=bibit, **validated_data)
        return investasi